package com.clearos.dlt.fido;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

public class AddHeaderInterceptor implements Interceptor {
    @Override @NotNull public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(
                chain.request().newBuilder()
                        .header("X-Requested-With", "XMLHttpRequest")
                        .build()
        );
    }
}
