package com.clearos.dlt.fido;

import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialRequestOptions;
import okhttp3.Call;

public interface AuthenticateBeginCallback {
    void onSuccess(PublicKeyCredentialRequestOptions options, String challenge);
    void onFailure(Call call, Exception e);
}
