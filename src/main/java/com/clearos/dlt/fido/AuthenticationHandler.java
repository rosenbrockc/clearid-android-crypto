package com.clearos.dlt.fido;

import androidx.lifecycle.LiveData;

import com.google.android.gms.fido.fido2.Fido2PendingIntent;

import okhttp3.Call;

public interface AuthenticationHandler extends ListKeysCallback {
    void onAuthBeginSuccess(LiveData<Fido2PendingIntent> intent);
    void onAuthCompleteStarted(Call apiCall);
}
