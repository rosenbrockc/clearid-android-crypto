package com.clearos.dlt.fido;

public class Credential {
    public String id;
    public String publicKey;

    public Credential(String cId, String cPublicKey) {
        id = cId;
        publicKey = cPublicKey;
    }
}
