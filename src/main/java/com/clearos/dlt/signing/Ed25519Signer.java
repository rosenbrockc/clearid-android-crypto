package com.clearos.dlt.signing;
// Adapted from https://github.com/bcgov/http-did-auth-proxy
import com.goterl.lazycode.lazysodium.LazySodiumAndroid;
import com.goterl.lazycode.lazysodium.Sodium;
import com.goterl.lazycode.lazysodium.SodiumAndroid;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.interfaces.Sign;

import org.bitcoinj.core.Base58;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.GeneralSecurityException;

public class Ed25519Signer extends Signer<byte[]> {

    private static Logger log = LoggerFactory.getLogger(Ed25519Signer.class);
    private static final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private static final String SIGNING_KEY_TYPE = "ed25519";
    private static final String ALGORITHM = "ed25519";

    public Ed25519Signer() {

    }

    static boolean supports(String signingKeyType) {

        return SIGNING_KEY_TYPE.equals(signingKeyType);
    }

    @Override
    public String algorithm() {

        return ALGORITHM;
    }

    @Override
    public byte[] signingKey(String signingKeyString) {

        return Base58.decode(signingKeyString);
    }

    @Override
    public byte[] sign(byte[] signingBytes, byte[] signingKey) throws GeneralSecurityException {
        return signInternal(signingBytes, signingKey);
    }

    private static byte[] signInternal(byte[] message, byte[] privateKey) throws GeneralSecurityException {
        byte[] signedMessage;

        try {
            signedMessage = lazySodium.randomBytesBuf(Sign.BYTES + message.length);
            boolean res = ((Sign.Native)lazySodium).cryptoSign(signedMessage, message, message.length, privateKey);

            if (!res) {
                throw new SodiumException("Could not sign your message.");
            }
        } catch (SodiumException e) {
            throw new GeneralSecurityException(e.getMessage(), e.getCause());
        }

        return signedMessage;
    }
}