package com.clearos.dlt.signing;
// Adapted from https://github.com/bcgov/http-did-auth-proxy

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.Headers;
import okhttp3.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUtil {

    private static final Logger log = LoggerFactory.getLogger(HttpUtil.class);

    private static final String HEADER_CONNECTION = "Connection";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String HEADER_DID = "Master-DID";
    private static final String HEADER_VERIFIED_DID = "Verified-DID";
    private static final String HEADER_DERIVED_SIGNER = "Signer-ID";

    public static final String[] REMOVE_HEADERS = new String[] { "Authorization", "Connection", "Proxy-Connection", "Via", "Host", "X-Forwarded-Host", "X-Forwarded-Proto", "Forwarded", "X-Forwarded-For", "X-Forwarded-Port" };

    public static Map<String, String> headers(Headers requestHeaders) {

        Map<String, String> headers = new HashMap<>();
        for (String name : requestHeaders.names()) {
            String value = String.join(", ", requestHeaders.values(name));
            if (log.isDebugEnabled()) log.debug("Header: " + name + " -> " + value);
            headers.put(name, value);
        }

        return headers;
    }

    public static List<String> signedHeaderNames(Map<String, String> headers) {

        List<String> signedHeaderNames = new ArrayList<>();
        //signedHeaderNames.add("(request-target)");
        for (String header : headers.keySet()) signedHeaderNames.add(header.toLowerCase());
        for (String removeHeader : REMOVE_HEADERS) signedHeaderNames.remove(removeHeader.toLowerCase());

        return signedHeaderNames;
    }

    public static Request setSignature(Request request, Signature signature, String masterDid) {
        String value = signature.toString();
        if (log.isDebugEnabled()) log.debug("Adding " + HEADER_AUTHORIZATION + " header: " + value);
        return request.newBuilder()
                .addHeader(HEADER_AUTHORIZATION, value)
                .build();
    }

    public static Signature getSignature(Map<String, String> headers) {
        String value = headers.get(HEADER_AUTHORIZATION);

        if (log.isDebugEnabled()) log.debug("Got " + HEADER_AUTHORIZATION + " header: " + value);

        return Signature.fromString(value);
    }
}